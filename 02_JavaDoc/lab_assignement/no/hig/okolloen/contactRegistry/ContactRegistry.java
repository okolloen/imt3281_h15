/**
 * 
 */
package no.hig.okolloen.contactRegistry;

import javax.swing.JOptionPane;

/**
 * The contact registry. Holds up to ten contacts, the user are asked to enter the contact information
 * when the program starts and then that information is shown on standard output.
 * 
 * No storing of the contact information to file or other fancy options :-).
 * 
 * @author Øivind Kolloen
 *
 */
public class ContactRegistry {
	Contact contacts[] = new Contact[10];
	
	/**
	 * Creates a new ContactRegistry object and starts the registration process.
	 */
	public ContactRegistry() {
		registerContacts();
		System.out.println(this);
	}

	/** 
	 * Returns a list of all the contacts registered.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// Use StringBuffer when adding many strings together
		StringBuffer sb = new StringBuffer();
		for (Contact c: contacts) {
			if (c!=null) {
				sb.append(c);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	/**
	 * Method used to register contacts. Asks user for family name, given name and phone number.
	 * If all information is entered a new contact is created and the user is asked if he wants to add another contact.
	 * If any of the information is missing the user is asked if he wants to try again.
	 * A maximum of ten contacts are added.  
	 */
	private void registerContacts() {
		boolean registerMoreContacts = false;
		int contactsAdded = 0;
		do {	// Try to add at least one contact
			String given=null, phone=null;
			String family = JOptionPane.showInputDialog("Family name : ");
			if (family != null) {
				given = JOptionPane.showInputDialog("Given name : ");
			}
			if (given != null) {
				phone = JOptionPane.showInputDialog("Phone number : ");
			}
			if (phone != null) {			// All data is correct, add the contact
				contacts[contactsAdded++] = new Contact (given, family, phone);
				registerMoreContacts = JOptionPane.showConfirmDialog(null, "Add more contacts?")==JOptionPane.YES_OPTION;
			} else {
				registerMoreContacts = JOptionPane.showConfirmDialog(null, "Try again?")==JOptionPane.YES_OPTION;
			}
		} while (registerMoreContacts);		// Add contacts until the user indicates no more contacts
	}

	/**
	 * Starts the program.
	 * 
	 * @param args unused, only here to adhere to convention 
	 */
	public static void main(String[] args) {
		new ContactRegistry();
	}

}
