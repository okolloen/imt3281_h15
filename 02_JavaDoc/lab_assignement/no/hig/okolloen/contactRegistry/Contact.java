package no.hig.okolloen.contactRegistry;

/**
 * Objects of this class will store our contacts. 
 * Apart from the three string definitions everything here is auto generated.
 * 
 * @author �ivind Kolloen
 *
 */
public class Contact {
	private String givenName;
	private String familyName;
	private String phoneNumber;

	/**
	 * Create a new, empty Contact object.
	 */
	public Contact() {
	}

	/**
	 * Create a new Contact object with the given name and contact details.
	 * 
	 * @param givenName
	 * @param familyName
	 * @param phoneNumber
	 */
	public Contact(String givenName, String familyName, String phoneNumber) {
		this.givenName = givenName;
		this.familyName = familyName;
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * @param familyName the familyName to set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Contact [givenName=" + givenName + ", familyName=" + familyName
				+ ", phoneNumber=" + phoneNumber + "]";
	}
}
