package no.hig.okolloen.realProgram;

import javax.swing.JOptionPane;

public class GUI {

	public GUI () {
		String first = JOptionPane.showInputDialog("Your first name ?");
		String last = JOptionPane.showInputDialog("Your last name ?");
		Person p = new Person (first, last);
		JOptionPane.showMessageDialog(null, "Welcome "+p);
	}
	
	public static void main(String[] args) {
		new GUI ();
	}
}
