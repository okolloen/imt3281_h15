import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class HelloWorldGUI2 {

	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, 
										new ImageIcon("world.png"), "Hello World", 
										JOptionPane.PLAIN_MESSAGE);
	}
}
