import javax.swing.JOptionPane;


public class UserInput {

	public static void main(String[] args) {
		String name = JOptionPane.showInputDialog("What is your name?");
		JOptionPane.showMessageDialog(null, "Hi there "+name);
	}
}
