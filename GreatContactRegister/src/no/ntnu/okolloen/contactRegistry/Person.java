package no.ntnu.okolloen.contactRegistry;

import java.io.Serializable;

public class Person implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    String fname, lname, number, bdate;

    public Person(String text, String text2, String text3, String text4) {
        fname = text;
        lname = text2;
        number = text3;
        bdate = text4;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Person [fname=" + fname + ", lname=" + lname + ", number=" + number + ", bdate="
                + bdate + "]";
    }

}
