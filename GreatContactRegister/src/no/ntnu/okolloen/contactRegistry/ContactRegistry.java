/**
 * 
 */
package no.ntnu.okolloen.contactRegistry;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.ntnu.okolloen.I18N.I18N;

/**
 * @author oivindk
 *
 */
public class ContactRegistry extends JFrame {
    DefaultListModel<Person> contactList = new DefaultListModel<>();
    JList<Person> contactsList = new JList<>(contactList);

    public ContactRegistry() {
        super(I18N.getBundle().getString("window.title"));
        setJMenuBar(createMenu());
        JTabbedPane tabs = new JTabbedPane();
        tabs.add(I18N.getBundle().getString("tabs.add"), new AddPanel(contactList));
        tabs.add(I18N.getBundle().getString("tabs.list"), createListTab());
        tabs.add(I18N.getBundle().getString("tabs.search"), createSearchTab());
        add(tabs);
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        readContacts();
    }

    /**
     * Reads the contents of the contact.dta and fills inn the contactList.
     */
    private void readContacts() {
        try {
            FileInputStream fis = new FileInputStream("contacts.dta");
            ObjectInputStream ois = new ObjectInputStream(fis);
            while (true) {
                Person p = (Person) ois.readObject();
                contactList.addElement(p);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (EOFException eof) {

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Component createSearchTab() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Creates a panel with a list that shows all registered contacts.
     * 
     * @return a JPanel with a list showing all registered contacts
     */
    private Component createListTab() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(contactsList, BorderLayout.CENTER);
        // TODO Auto-generated method stub
        return panel;
    }

    private JMenuBar createMenu() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        new ContactRegistry();

    }

}
