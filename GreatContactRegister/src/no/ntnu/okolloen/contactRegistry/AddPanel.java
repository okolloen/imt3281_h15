package no.ntnu.okolloen.contactRegistry;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.ntnu.okolloen.I18N.I18N;

/**
 * The contents of the addContact tab in the contact registry.
 * 
 * @author oivindk
 *
 */
public class AddPanel extends JPanel implements ActionListener {
    private JTextField fname = new JTextField(30);
    private JTextField lname = new JTextField(30);
    private JTextField number = new JTextField(30);
    private JTextField bdate = new JTextField(30);
    private DefaultListModel<Person> contactsList;
    private GridBagLayout layout = new GridBagLayout();
    private GridBagConstraints constraints = new GridBagConstraints();

    /**
     * Default constructor, adds components to the panel and adds an action
     * listener to the button.
     */
    public AddPanel() {
        setLayout(layout);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 1;

        addLabelAndTextField(1, fname, "tabs.add.fname");
        addLabelAndTextField(2, lname, "tabs.add.lname");
        addLabelAndTextField(3, number, "tabs.add.number");
        addLabelAndTextField(4, bdate, "tabs.add.bdate");

        JButton addButton = new JButton(I18N.getBundle().getString("tabs.add.addButton"));
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridy = 10;
        layout.setConstraints(addButton, constraints);
        add(addButton);

        addButton.addActionListener(this);
    }

    /**
     * Called when the user press the button to add a contact. Adds a new
     * contact to the contacts.dta file and also the the active list of
     * contacts.
     * 
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Person p = new Person(fname.getText(), lname.getText(), number.getText(), bdate.getText());
        try {
            FileOutputStream fos = new FileOutputStream(new File("contacts.dta"), true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(p);
            oos.close();
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        fname.setText("");
        lname.setText("");
        number.setText("");
        bdate.setText("");
        contactsList.addElement(p);
    }

    /**
     * Create a new panel for adding contacts. Take the list model for the
     * contact list as a parameter and store a reference to it so that we can
     * add the contacts added to file to the list as well (for display
     * purposes.)
     * 
     * @param contactsList
     *            a reference to the list of contacts
     */
    public AddPanel(DefaultListModel<Person> contactsList) {
        this();
        this.contactsList = contactsList;
    }

    /**
     * @param row
     *            which row to add this label and text field to
     * @param field
     *            the textfield to be added
     * @param key
     *            a key that references a value in the properties for this
     *            application
     */
    private void addLabelAndTextField(int row, JTextField field, String key) {
        JLabel label = new JLabel(I18N.getBundle().getString(key));
        constraints.gridy = (row - 1) * 2 + 1;
        layout.setConstraints(label, constraints);
        add(label);
        constraints.gridy = (row - 1) * 2 + 2;
        layout.setConstraints(field, constraints);
        add(field);
    }
}
