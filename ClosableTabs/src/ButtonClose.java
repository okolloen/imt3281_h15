import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicButtonUI;

public class ButtonClose extends JPanel {
    String title;

    public ButtonClose(final String title, Icon icon, MouseListener e) {
        this.title = title;
        setOpaque(false);
        JLabel text = new JLabel(title);
        text.setOpaque(false);
        text.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

        ButtonTab button = new ButtonTab();
        button.setOpaque(false);
        button.addMouseListener(e);
        button.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        JPanel p = new JPanel();
        p.setOpaque(false);
        p.setSize(getWidth(), 15);
        p.add(text);
        p.add(button);

        add(p);
    }

    private class ButtonTab extends JButton {

        public ButtonTab() {
            int size = 13;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("Close");

            setUI(new BasicButtonUI());

            setFocusable(false);
            setBorderPainted(false);

            addMouseListener(listener);
            setRolloverEnabled(true);
        }

        @Override
        public void updateUI() {
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();

            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }
            g2.setStroke(new BasicStroke(2));
            g2.setColor(new Color(126, 118, 91));

            if (getModel().isRollover()) {
                g2.setColor(Color.WHITE);
            }

            int delta = 3;
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight()
                    - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight()
                    - delta - 1);
            g2.dispose();
        }
    }

    private final MouseListener listener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setContentAreaFilled(true);
                button.setBackground(new Color(215, 65, 35));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setContentAreaFilled(false); // transparent
            }
        }
    };
}