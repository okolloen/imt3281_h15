import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Main extends JFrame {
    JTabbedPane pane = new JTabbedPane();
    JLabel status = new JLabel(" ");

    public Main() {
        super("Closable tabs");
        JTextField name = new JTextField();
        name.addActionListener(e -> {
            pane.add(name.getText(), new JLabel(name.getText()));
            addTag(pane, name.getText(), null, pane.getTabCount() - 1);
            name.setText("");
        });
        add(name, BorderLayout.NORTH);
        add(pane, BorderLayout.CENTER);
        add(status, BorderLayout.SOUTH);
        setSize(600, 500);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void addTag(JTabbedPane tab, String title, Icon icon, int index) {
        MouseListener close = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                // status.setText(e.getSource().toString());
                ButtonClose c = (ButtonClose) ((Component) e.getSource())
                        .getParent().getParent();
                System.out.println(c.title);
                System.out.println(pane.indexOfTab(c.title));
                ;
            }

        };
        final ButtonClose buttonClose = new ButtonClose(title, icon, close);

        tab.setTabComponentAt(index, buttonClose);
        tab.validate();
        tab.setSelectedIndex(index);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager
                    .getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Main();
    }
}
