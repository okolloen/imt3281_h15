/**
 * 
 */
/**
 * @author oivindk
 * 
 * Contains example code showing how to format number & currencies, 
 * Dates and times and more advanced message formating.
 * 
 * Code from : https://docs.oracle.com/javase/tutorial/i18n/format/index.html
 *
 */
package formatting;