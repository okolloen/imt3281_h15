/**
 * 
 */
/**
 * @author oivindk
 * 
 * Containing the first internationalization examples.
 * Code is from https://docs.oracle.com/javase/tutorial/i18n/intro/quick.html
 */
package introExample;