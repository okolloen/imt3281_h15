# IMT3281 Software development #

This repository holds all source code used during the lectures in IMT3281 Software development at Gjøvik University College during the fall semester of 2015.

### Whats here ###

* All the code that I will go trough during the lectures are held in this repository. Most of the code is from the textbook "Java - How to program" from deitel & deitel.
* Do a checkout before each lecture to be sure that you have the latest version of the code. The different parts are held in different folders "XX_SubjectName" so they should be easy to identify [more information about the lectures](https://bitbucket.org/okolloen/imt3281_h15/wiki/Home).

### How do I get set up? ###

* If you are using Eclipse you can import the repository into a blank work space. Open up Eclipse with a new work space and right click in the projects part and select import, then follow the wizard.
* For a plain command line version, use git to clone the repository into a blank directory and you are up and running. 

### Question or comments? ###

* If you have any questions feel free to contact me at okolloen(at)hig.no
* If you find an error or have suggestions for improvements, please contact me at okolloen(at)hig.no