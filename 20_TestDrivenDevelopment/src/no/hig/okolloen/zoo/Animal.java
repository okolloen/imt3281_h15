package no.hig.okolloen.zoo;

import java.io.Serializable;

public class Animal implements Serializable {
    private String name;

    public Animal() {
        name = "";
    }

    public Animal(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
