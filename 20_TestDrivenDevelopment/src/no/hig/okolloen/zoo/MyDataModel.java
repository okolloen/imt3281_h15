package no.hig.okolloen.zoo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class MyDataModel extends AbstractTableModel {
    ArrayList<Animal> data = new ArrayList<Animal>();

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Animal animal = data.get(rowIndex);
        if (columnIndex == 1)
            return animal.getName();
        else if (columnIndex == 0) {
            if (animal instanceof Lion)
                return "Lion";
            else if (animal instanceof Animal)
                return "Animal";
        }
        return false;
    }

    public void append(Animal animal) {
        data.add(animal);
    }

    public void saveToFile(ObjectOutputStream oos) throws IOException {
        oos.writeObject(data);
    }

    public void loadFromFile(ObjectInputStream ois) throws IOException,
            ClassNotFoundException {
        data = (ArrayList<Animal>) ois.readObject();

    }

}
