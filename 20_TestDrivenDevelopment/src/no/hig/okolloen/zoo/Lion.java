package no.hig.okolloen.zoo;

public class Lion extends Animal {
    private boolean hungry;

    public Lion(String name) {
        super(name);
    }

    public Lion() {
        super();
    }

    public void setHungry(boolean hungry) {
        this.hungry = hungry;
    }

    public boolean getHungry() {
        return hungry;
    }

    public boolean eats(Animal animal) {
        if (animal instanceof Lion)
            return false;
        return hungry;
    }

}
