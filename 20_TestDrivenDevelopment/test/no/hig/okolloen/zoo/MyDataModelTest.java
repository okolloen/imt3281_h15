package no.hig.okolloen.zoo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

public class MyDataModelTest {

    @Test
    public void testConstructor() {
        MyDataModel dataModel = new MyDataModel();
        assertEquals(0, dataModel.getRowCount());
        assertEquals(2, dataModel.getColumnCount());
    }

    @Test
    public void testInsertAnimal() {
        MyDataModel dataModel = new MyDataModel();
        dataModel.append(new Animal("Kanga"));
        assertEquals(1, dataModel.getRowCount());
    }

    @Test
    public void testGetValueAt() {
        MyDataModel dataModel = new MyDataModel();
        dataModel.append(new Animal("Kanga"));
        assertEquals("Kanga", dataModel.getValueAt(0, 1));
        assertEquals("Animal", dataModel.getValueAt(0, 0));
        dataModel.append(new Lion("Alex"));
        assertEquals("Alex", dataModel.getValueAt(1, 1));
        assertEquals("Lion", dataModel.getValueAt(1, 0));
    }

    @Test
    public void loadAndSaveTest() {
        MyDataModel dataModel = new MyDataModel();
        dataModel.append(new Animal("Kanga"));
        dataModel.append(new Lion("Alex"));
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            dataModel.saveToFile(oos);
            oos.flush();
            MyDataModel dataModel1 = new MyDataModel();
            ByteArrayInputStream bais = new ByteArrayInputStream(
                    baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            dataModel1.loadFromFile(ois);
            assertEquals("Alex", dataModel1.getValueAt(1, 1));
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }
}
