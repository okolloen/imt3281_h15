package no.hig.okolloen.zoo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LionTest {

    @Test
    public void testConstructor() {
        Lion lion = new Lion();
        assertTrue(lion instanceof Animal);
        assertEquals("", lion.getName());
        lion = new Lion("Alex");
        assertEquals("Alex", lion.getName());
    }

    @Test
    public void testHungryLion() {
        Lion lion = new Lion("Alex");
        lion.setHungry(false);
        assertFalse(lion.getHungry());
        Animal animal = new Animal("Kanga");
        assertFalse(lion.eats(animal));
        lion.setHungry(true);
        assertTrue(lion.getHungry());
        assertTrue(lion.eats(animal));
        Lion lion1 = new Lion("Simba");
        assertFalse(lion.eats(lion1));
    }
}
