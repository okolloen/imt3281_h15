package no.hig.okolloen.zoo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AnimalTest {

    @Test
    public void testConstructor() {
        Animal animal = new Animal();
        assertEquals("", animal.getName());
        animal = new Animal("Kanga");
        assertEquals("Kanga", animal.getName());
    }

    @Test
    public void testGettersAndSetters() {
        Animal animal = new Animal();
        animal.setName("Kanga");
        assertEquals("Kanga", animal.getName());
    }

}
