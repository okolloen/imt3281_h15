// An interface that defines a single method is a functional interface
// Functional interfaces are a central part in using Lambda functions 
interface Operation {
	// Single method interface, functional interface
	public int perform (int x, int y);
}


class Runner {
	// The run method accepts an object of class Operation as its parameter
	public void run (Operation o) {
		System.out.println("The sum is " + o.perform(12, 15));
	}
}

public class LambdaBasics {
	public static void main(String[] args) {
		Runner runner = new Runner();
		// The old way
		runner.run(new Operation () {
			public int perform (int x, int y) {
				System.out.println("adding "+x+" to "+y);
				return x+y;
			}
		});
		
		// With Lambda, a and b is of type int because of the definition of the interface
		// That is, the method run expects an object of type Operation and that
		// interface defines one method that takes two parameters of type int.
		runner.run((a, b) -> {
			System.out
					.println("adding " + a + " to " + b + " using lambdas.");
			return a+b;
		});
	}
}