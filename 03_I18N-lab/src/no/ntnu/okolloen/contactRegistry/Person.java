package no.ntnu.okolloen.contactRegistry;

import java.util.Date;

import no.ntnu.okolloen.I18N.I18N;

public class Person {
    String fname, lname;
    String number;
    Date birthdate;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        Object data[] = { fname, lname, number, birthdate };

        return I18N.getConcatenatedString("PersonToString", data);
    }

    public Person() {

    }

    public Person(String fname, String lname, String number, Date birthdate) {
        super();
        this.fname = fname;
        this.lname = lname;
        this.number = number;
        this.birthdate = birthdate;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname
     *            the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname
     *            the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number
     *            the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * @param birthdate
     *            the birthdate to set
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

}
