package no.ntnu.okolloen.contactRegistry;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import javax.swing.JOptionPane;

import no.ntnu.okolloen.I18N.I18N;

public class ContactRegistry {
    Person persons[] = new Person[10];
    int registeredContacts = 0;

    public ContactRegistry() {
        boolean addMore = false;
        do {
            Person p = getPersonData();
            if (p != null) {
                persons[registeredContacts] = p;
                registeredContacts++;
                addMore = JOptionPane.showConfirmDialog(null,
                        I18N.getBundle().getString(
                                "addMoreContacts")) == JOptionPane.YES_OPTION;
            } else {
                addMore = JOptionPane.showConfirmDialog(null, I18N.getBundle()
                        .getString("errorInContact")) == JOptionPane.YES_OPTION;
            }
        } while (registeredContacts < 10 && addMore);
        System.out.println(this);
    }

    @Override
    public String toString() {
        String tmp = "";
        for (Person p : persons) {
            if (p != null) {
                tmp += p;
                tmp += "\n";
            }
        }
        Object data[] = { new Integer(registeredContacts) };
        tmp += I18N.getPluralString("contactRegistryReport", "noContacts",
                "oneContact", "moreContacts", data);
        return tmp;
    }

    /**
     * @return
     */
    private Person getPersonData() {
        String fname = JOptionPane
                .showInputDialog(I18N.getBundle().getString("fname"));
        if (fname == null || fname.equals("")) {
            return null;
        }
        String lname = JOptionPane
                .showInputDialog(I18N.getBundle().getString("lname"));
        if (lname == null || lname.equals("")) {
            return null;
        }
        String number = JOptionPane
                .showInputDialog(I18N.getBundle().getString("phone"));
        if (number == null || number.equals("")) {
            return null;
        }
        Object data[] = { new Date() };
        String bdate = JOptionPane
                .showInputDialog(I18N.getConcatenatedString("bdate", data));
        if (bdate == null || bdate.equals("")) {
            return null;
        }
        DateFormat dformat = DateFormat.getDateInstance(DateFormat.SHORT,
                I18N.getBundle().getLocale());
        dformat.setLenient(true);
        Date bdate1 = null;
        try {
            bdate1 = dformat.parse(bdate);
        } catch (ParseException e) {
            return null;
        }
        Person p = new Person(fname, lname, number, bdate1);
        return p;
    }

    public static void main(String[] args) {
        if (args.length == 2) {
            I18N.setLanguageCountry(args[0], args[1]);
        } else if (args.length == 1) {
            I18N.setLanguage(args[0]);
        }
        new ContactRegistry();
    }
}
