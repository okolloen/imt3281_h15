/**
 * 
 */
package no.ntnu.okolloen.I18N;

import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * All I18N handled through this class.
 * 
 * @author oivindk
 *
 */
public class I18N {
    private static I18N i18n = new I18N();
    private static ResourceBundle bundle;

    private I18N() {
        I18N.bundle = ResourceBundle.getBundle("no.ntnu.okolloen.I18N.i18n",
                Locale.getDefault());
    }

    /**
     * Allow override of language
     * 
     * @param language
     */
    public static void setLanguage(String language) {
        Locale locale = new Locale(language);
        I18N.bundle = ResourceBundle.getBundle("no.ntnu.okolloen.I18N.i18n",
                locale);
    }

    public static void setLanguageCountry(String language, String country) {
        Locale locale = new Locale(language, country);
        I18N.bundle = ResourceBundle.getBundle("no.ntnu.okolloen.I18N.i18n",
                locale);
    }

    /**
     * Get resource bundle
     * 
     * @return ResourceBundle for application
     */
    public static ResourceBundle getBundle() {
        return bundle;
    }

    /**
     * Gets a concatenated string. Inserts data from the provided array of
     * objects into the pattern pointed to by the pattern parameter
     * 
     * @param pattern
     *            a string pointing to the pattern to be used
     * @param data
     *            an array of object to insert into a pattern
     * 
     * @return a String where data is inserted into pattern
     */
    public static String getConcatenatedString(String pattern, Object data[]) {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(I18N.getBundle().getLocale());
        formatter.applyPattern(I18N.getBundle().getString(pattern));
        return formatter.format(data);
    }

    public static String getPluralString(String pattern, String none,
            String one, String more, Object data[]) {
        MessageFormat messageForm = new MessageFormat("");
        messageForm.setLocale(I18N.bundle.getLocale());
        double[] limits = { 0, 1, 2 };
        String[] strings = { bundle.getString(none), bundle.getString(one),
                bundle.getString(more) };
        ChoiceFormat choiceForm = new ChoiceFormat(limits, strings);
        messageForm.applyPattern(bundle.getString(pattern));
        Format[] formats = { choiceForm, null, NumberFormat.getInstance() };
        messageForm.setFormats(formats);
        return messageForm.format(data);
    }
}
