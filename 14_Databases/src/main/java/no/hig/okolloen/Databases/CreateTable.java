package no.hig.okolloen.Databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * @see CreateDatabase for the code to create the database. Here we create a table in the newly created database.
 */
public class CreateTable {
    // Connect using jdbc, and the derby subprotocol, connect to database
    // MyDbTest and, create the database as well (scary stuff, can overwrite
    // existing DB)
    private final static String url = "jdbc:derby:MyDbTest;create=true";

    public static void main(String[] args) {
        try {
            // This is just as any other database
            Connection con = DriverManager.getConnection(url);
            // Create a statement
            Statement stmt = con.createStatement();
            // use execute to perform CREATE, INSERT, UPDATE, DELETE,
            // executeQuery is for SELECT
            // MySQL has auto_increment, Derby has GENERATED values
            stmt.execute("CREATE TABLE contacts (id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                    + "givenname varchar(128) NOT NULL, "
                    + "surename varchar(128) NOT NULL, "
                    + "email varchar(128) NOT NULL, "
                    + "phone varchar(32) default NULL, " + "PRIMARY KEY  (id))");
            // And close
            con.close();
            System.out.println("Table 'contacts' created");
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
}