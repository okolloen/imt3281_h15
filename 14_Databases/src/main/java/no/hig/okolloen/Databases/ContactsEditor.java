package no.hig.okolloen.Databases;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/*
 * A simple editor for my contacts database.
 * Uses a JTable as the editor, nothing fancy. The data can be sorted on each of the columns.
 */
@SuppressWarnings("serial")
public class ContactsEditor extends JFrame {
    // A reference to the table model
    DatabaseTableModel tableModel;
    
    public ContactsEditor() {
        super ("Editor for my contacts");
        try {
            // Create the table model, this will connect to the database and perform the query to fill the table model
            tableModel = new DatabaseTableModel();
        } catch (SQLException sqle) {
            // If an error occured we just end everything
            JOptionPane.showMessageDialog(null, sqle.getMessage());
            System.exit(1);
        }
        // Use a JTable object to show the data from the table model
        JTable table = new JTable(tableModel);
        add (new JScrollPane(table));
        // Add the ability to sort on an arbitrary column
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
        table.setRowSorter(sorter);
        // Set the size of the editor
        setSize(800, 600);
        setVisible (true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Before we exit the application, we make sure that the database connection
        // is closed properly.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                tableModel.disconnectFromDatabase();
            }
        });
    }
    
    public static void main(String[] args) {
        new ContactsEditor();
    }
}
