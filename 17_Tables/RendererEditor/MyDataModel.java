
/**
 * 
 */

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * @author oivindk
 *
 */
public class MyDataModel extends AbstractTableModel {
    Vector<Entry> data = new Vector<Entry>();
    String columnNames[] = { "Name", "Mood", "Reason" };

    @Override
    public int getRowCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public int getColumnCount() {
        // TODO Auto-generated method stub
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Entry e = data.get(rowIndex);
        switch (columnIndex) {
        case 0: {
            return e.getName();
        }
        case 1: {
            return e.getMood();
        }
        case 2: {
            return e.getReason();
        }
        }
        return null;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Entry e = data.get(rowIndex);
        switch (columnIndex) {
        case 0: {
            e.setName(value.toString());
            break;
        }
        case 1: {
            e.setMood((Integer) value);
            break;
        }
        case 2: {
            e.setReason((String) value);
            break;
        }
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    public void addRow() {
        Entry e = new Entry(1, "", "");
        data.addElement(e);
        // Let listeners know we added one row (the last row.)
        fireTableRowsInserted(data.size()-1, data.size()-1);
    }

    class Entry {
        int mood;
        String name, reason;

        /**
         * @param mood
         * @param name
         * @param reason
         */
        public Entry(int mood, String name, String reason) {
            super();
            this.mood = mood;
            this.name = name;
            this.reason = reason;
        }

        /**
         * @return the mood
         */
        public int getMood() {
            return mood;
        }

        /**
         * @param mood
         *            the mood to set
         */
        public void setMood(int mood) {
            this.mood = mood;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name
         *            the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the reason
         */
        public String getReason() {
            return reason;
        }

        /**
         * @param reason
         *            the reason to set
         */
        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
